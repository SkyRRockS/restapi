package com.example.rest.dto;

public class ArticleCreateDtoIn {
    public String name;

    public int numberOwned;

    public String description;

    public float price;
}
