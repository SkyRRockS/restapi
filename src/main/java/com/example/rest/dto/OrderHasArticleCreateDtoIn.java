package com.example.rest.dto;

public class OrderHasArticleCreateDtoIn {
    public long articleId;

    public long orderId;

    public int quantity;
}
