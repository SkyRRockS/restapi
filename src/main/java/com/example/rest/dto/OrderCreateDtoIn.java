package com.example.rest.dto;

import java.util.Date;

public class OrderCreateDtoIn {

    public Date dateOrdered;

    public ArticleForOrder[] articles;
}
