package com.example.rest.dto;

public class ArticleDtoIn {
    public Long articleId;

    public String name;

    public int numberOwned;

    public String description;

    public float price;
}
