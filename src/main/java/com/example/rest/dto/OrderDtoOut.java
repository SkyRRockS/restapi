package com.example.rest.dto;

import java.util.Date;
import java.util.List;

public class OrderDtoOut {
    private long id;

    private Date date;

    private float totalPrice;

    private List<OrderArticleDtoOut> articles;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<OrderArticleDtoOut> getArticles() {
        return articles;
    }

    public void setArticles(List<OrderArticleDtoOut> articles) {
        this.articles = articles;
    }
}
