package com.example.rest.dto;

public class ArticleForOrder {

    public Long articleId;

    public int quantity;

    public float price;
}
