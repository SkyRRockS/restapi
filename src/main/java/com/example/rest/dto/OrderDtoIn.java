package com.example.rest.dto;

import java.util.Date;

public class OrderDtoIn {
    public long orderId;

    public float totalPrice;

    public Date dateOrdered;
}
