package com.example.rest.dto;

public class OrderHasArticleDtoIn {

    public long id;

    public long articleId;

    public long orderId;

    public int quantity;
}
