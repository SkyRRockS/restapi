package com.example.rest.repositories;

import com.example.rest.entities.OrderHasArticle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderHasArticleRepository extends JpaRepository<OrderHasArticle, Long> {
}
