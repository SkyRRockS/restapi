package com.example.rest.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "orderHasArticle")
public class OrderHasArticle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int quantity;

    public long getId() {
        return id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Article getArticle() {
        return articles;
    }

    public void setArticle(Article article) {
        this.articles = article;
    }

    public Order getOrder() {
        return orders;
    }

    public void setOrder(Order order) {
        this.orders = order;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articles.id")
    private Article articles;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orders.id")
    private Order orders;
}
