package com.example.rest.services;

import com.example.rest.dto.ArticleCreateDtoIn;
import com.example.rest.dto.ArticleDtoIn;
import com.example.rest.entities.Article;
import com.example.rest.repositories.ArticlesRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service()
public class ArticleService {
    private final ArticlesRepository articlesRepository;

    public ArticleService(ArticlesRepository articlesRepository) {
        this.articlesRepository = articlesRepository;
    }


    public void createArticle(ArticleCreateDtoIn dtoIn) {
        Article articleToSave = new Article();
        if (dtoIn.name != null && !dtoIn.name.trim().isEmpty()) articleToSave.setName(dtoIn.name);
        if (dtoIn.description != null && !dtoIn.description.trim().isEmpty()) articleToSave.setDescription(dtoIn.description);
        if (dtoIn.price >= 0) articleToSave.setPrice(dtoIn.price);
        if (dtoIn.numberOwned >= 0) articleToSave.setNumberOwned(dtoIn.numberOwned);
        articleToSave.setDeleted(false);
        articlesRepository.save(articleToSave);
    }

    public void updateArticle(ArticleDtoIn dtoIn, Long id) {
        Article articleToSave = articlesRepository.findById(id).get();
        if (dtoIn.name != null && !dtoIn.name.trim().isEmpty()) articleToSave.setName(dtoIn.name);
        if (dtoIn.description != null && !dtoIn.description.trim().isEmpty()) articleToSave.setDescription(dtoIn.description);
        if (dtoIn.numberOwned >= 0) articleToSave.setNumberOwned(dtoIn.numberOwned);
        if (dtoIn.price >= 0) articleToSave.setPrice(dtoIn.price);
        articlesRepository.save(articleToSave);
    }

    public void setArticleNotOrderable(Long id) {
        Article articleToSave = articlesRepository.findById(id).get();
        articleToSave.setDeleted(true);
        articlesRepository.save(articleToSave);
    }

    public void updateQuantityStock(Article article, int quantity, boolean isPositive) {
        int tempQuantity = article.getNumberOwned() - quantity;
        article.setNumberOwned(tempQuantity);
        System.out.println(article.getNumberOwned());

        articlesRepository.save(article);
    }

    public void updatePrice(Long id, float price) {
        Article articleToUpdate = articlesRepository.findById(id).get();
        articleToUpdate.setPrice(price);
        articlesRepository.save(articleToUpdate);
    }

    public boolean checkQuantity(Long id, int quantity) {
        Article article = articlesRepository.findById(id).get();
        return (article.getNumberOwned() - quantity) >= 0;
    }

    public void deleteArticle(Long id) {
        articlesRepository.deleteById(id);
    }

    public boolean checkIsOrderable(Long id) {
        Article articleToCheck = articlesRepository.findById(id).get();
        return !articleToCheck.isDeleted();
    }

    public Optional<Article> getById(Long id) { return articlesRepository.findById(id); }

    public List<Article> getAll() { return articlesRepository.findAll(); }

    public List<Article> getAllOrderable() {
        List<Article> articleList = articlesRepository.findAll();
        List<Article> finalArticles = new ArrayList<Article>();
        for ( int i = 0; i < articleList.size(); i++) {
            if (!articleList.get(i).isDeleted()) finalArticles.add(articleList.get(i));
        }
        return finalArticles;
    }
}
