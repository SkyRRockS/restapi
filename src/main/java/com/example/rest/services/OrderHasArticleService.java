package com.example.rest.services;

import com.example.rest.dto.OrderHasArticleCreateDtoIn;
import com.example.rest.dto.OrderHasArticleDtoIn;
import com.example.rest.entities.Article;
import com.example.rest.entities.Order;
import com.example.rest.entities.OrderHasArticle;
import com.example.rest.repositories.OrderHasArticleRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service()
public class OrderHasArticleService {
   private final OrderHasArticleRepository orderHasArticleRepository;
    private final ArticleService articleService;

    public OrderHasArticleService(OrderHasArticleRepository orderHasArticleRepository, ArticleService articleService) {
        this.orderHasArticleRepository = orderHasArticleRepository;
        this.articleService = articleService;
    }

    public float createOrderHasArticle(OrderHasArticleCreateDtoIn dtoIn, Order order) {
        OrderHasArticle orderHasArticleToSave = new OrderHasArticle();
        Article articleLinked = articleService.getById(dtoIn.articleId).get();
        orderHasArticleToSave.setArticle(articleLinked);
        orderHasArticleToSave.setOrder(order);
        orderHasArticleToSave.setQuantity(dtoIn.quantity);
        orderHasArticleRepository.save(orderHasArticleToSave);
        articleService.updateQuantityStock(articleLinked, dtoIn.quantity, true);
        return dtoIn.quantity * articleLinked.getPrice();
    }

    public List<OrderHasArticle> getForAnOrder(long id) {
        List<OrderHasArticle> orderHasArticles = orderHasArticleRepository.findAll();
        List<OrderHasArticle> finalorderHasArticles = new ArrayList<OrderHasArticle>();
        for ( int i = 0; i < orderHasArticles.size(); i++) {
            if ((orderHasArticles.get(i).getOrder().getId() == id)) {
                finalorderHasArticles.add(orderHasArticles.get(i));
            }
        }
        return finalorderHasArticles;
    }

    public void updateOrderHasArticle(OrderHasArticleDtoIn dtoIn, Order order) {
        OrderHasArticle orderHasArticleToSave = orderHasArticleRepository.getReferenceById(dtoIn.id);
        Article articleLinked = articleService.getById(dtoIn.articleId).get();
        orderHasArticleToSave.setArticle(articleLinked);
        orderHasArticleToSave.setOrder(order);
        orderHasArticleToSave.setQuantity(dtoIn.quantity);
        orderHasArticleRepository.save(orderHasArticleToSave);
    }

    public void deleteOrderHasArticle(Order order) {
        List<OrderHasArticle> orderHasArticles = orderHasArticleRepository.findAll();
        List<OrderHasArticle> orderToDelete = new ArrayList<OrderHasArticle>();
        orderHasArticles.forEach( orderHasArticleTo -> {
                long id =  orderHasArticleTo.getOrder().getId();
                if (id == order.getId()) {
                    orderToDelete.add(orderHasArticleTo);
                }
        });
        this.handleDeleting(orderToDelete);
    }

    public void handleDeleting(List<OrderHasArticle> orderToDelete) {
        orderToDelete.forEach( orderHasArticle -> {
            Article article = orderHasArticle.getArticle();
            this.articleService.updateQuantityStock(article, orderHasArticle.getQuantity(), false);
            this.orderHasArticleRepository.deleteById(orderHasArticle.getId());
        });
    }

    public void deleteOrderHasArticle(OrderHasArticleDtoIn dtoIn) { orderHasArticleRepository.deleteById(dtoIn.id); }

    public List<OrderHasArticle> getAll() { return orderHasArticleRepository.findAll(); }
}
