package com.example.rest.services;

import com.example.rest.dto.*;
import com.example.rest.entities.Article;
import com.example.rest.entities.Order;
import com.example.rest.entities.OrderHasArticle;
import com.example.rest.repositories.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service()
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderHasArticleService orderHasArticleService;

    private final ArticleService articleService;

    public OrderService(OrderRepository orderRepository, OrderHasArticleService orderHasArticleService, ArticleService articleService) {
        this.orderRepository = orderRepository;
        this.orderHasArticleService = orderHasArticleService;
        this.articleService = articleService;
    }

    public void createOrder(OrderCreateDtoIn dtoIn) {
        float price = 0.00F;
        boolean isOrderable = true;
        for (int i=0; i < dtoIn.articles.length; i++) {
            price += dtoIn.articles[i].price * dtoIn.articles[i].quantity;
            if (!articleService.checkQuantity(dtoIn.articles[i].articleId, dtoIn.articles[i].quantity)) {
                isOrderable = false;
            } else {
                isOrderable = articleService.checkIsOrderable(dtoIn.articles[i].articleId);
            }
        }
        if (!isOrderable) {
            throw new Error("quantity not orderable");
        } else {
            Order orderToSave = new Order();
            orderToSave.setTotalPrice(price);
            orderToSave.setOrderDate(dtoIn.dateOrdered);
            orderRepository.save(orderToSave);
            for (int i = 0; i < dtoIn.articles.length; i++) {
                OrderHasArticleCreateDtoIn oha = new OrderHasArticleCreateDtoIn();
                oha.orderId = orderToSave.getId();
                oha.quantity = dtoIn.articles[i].quantity;
                oha.articleId = dtoIn.articles[i].articleId;
                orderHasArticleService.createOrderHasArticle(oha, orderToSave);
            }
        }
    }

    public void updateOrder(OrderDtoIn dtoIn, Long id) {
        Order orderToSave = orderRepository.getReferenceById(id);
        orderToSave.setTotalPrice(dtoIn.totalPrice);
        orderToSave.setOrderDate(dtoIn.dateOrdered);
        orderRepository.save(orderToSave);
    }

    public void deleteOrder(Long id) {
        Order order = this.orderRepository.findById(id).get();
        orderHasArticleService.deleteOrderHasArticle(order);
        orderRepository.deleteById(id);
    }

    public OrderDtoOut getById(long id) {
        Order order = orderRepository.findById(id).get();
        List<OrderArticleDtoOut> orderArticle = new ArrayList<OrderArticleDtoOut>();
        OrderDtoOut orderToReturn = new OrderDtoOut();
        orderToReturn.setDate(order.getOrderDate());
        orderToReturn.setId(order.getId());
        orderToReturn.setTotalPrice(order.getTotalPrice());
        List<OrderHasArticle> orderHasArticles = orderHasArticleService.getForAnOrder(id);
        for (OrderHasArticle orderHasArticle : orderHasArticles) {
            Article article = articleService.getById(orderHasArticle.getArticle().getId()).get();
            OrderArticleDtoOut articleDtoOut = new OrderArticleDtoOut();
            articleDtoOut.setId(article.getId());
            articleDtoOut.setName(article.getName());
            articleDtoOut.setDescription(article.getDescription());
            articleDtoOut.setPrice(article.getPrice());
            articleDtoOut.setQuantity(orderHasArticle.getQuantity());
            orderArticle.add(articleDtoOut);
        }
        orderToReturn.setArticles(orderArticle);
        return orderToReturn;
    }

    public List<OrderDtoOut> getAll() {
        List<Order> orders = orderRepository.findAll();
        List<OrderDtoOut> ordersToSend = new ArrayList<OrderDtoOut>();
        orders.forEach(order -> {
            List<OrderArticleDtoOut> orderArticle = new ArrayList<OrderArticleDtoOut>();
            OrderDtoOut orderToReturn = new OrderDtoOut();
            orderToReturn.setDate(order.getOrderDate());
            orderToReturn.setId(order.getId());
            orderToReturn.setTotalPrice(order.getTotalPrice());
            List<OrderHasArticle> orderHasArticles = orderHasArticleService.getForAnOrder(order.getId());
            for (OrderHasArticle orderHasArticle : orderHasArticles) {
                Article article = articleService.getById(orderHasArticle.getArticle().getId()).get();
                OrderArticleDtoOut articleDtoOut = new OrderArticleDtoOut();
                articleDtoOut.setId(article.getId());
                articleDtoOut.setName(article.getName());
                articleDtoOut.setDescription(article.getDescription());
                articleDtoOut.setPrice(article.getPrice());
                articleDtoOut.setQuantity(orderHasArticle.getQuantity());
                orderArticle.add(articleDtoOut);
            }
            orderToReturn.setArticles(orderArticle);
            ordersToSend.add(orderToReturn);
        });
        return ordersToSend;
    }
}
