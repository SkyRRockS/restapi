package com.example.rest.controller;

import com.example.rest.dto.ArticleCreateDtoIn;
import com.example.rest.dto.ArticleDtoIn;
import com.example.rest.entities.Article;
import com.example.rest.services.ArticleService;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService service;

    public ArticleController(ArticleService service) {
        this.service = service;
    }

    @PostMapping()
    public void create(
            @RequestBody() ArticleCreateDtoIn dtoIn
    ) {
        service.createArticle(dtoIn);
    }

    @GetMapping("admin")
    public List<Article> getAllArticles(

    ) {
        return service.getAll();
    }

    @GetMapping()
    public List<Article> getAllArticlesOrderable(

    ) {
        return service.getAllOrderable();
    }

    @GetMapping("{id}")
    public ResponseEntity<Article> getOneArticle(
        @PathVariable Long id
    ) {
        return ResponseEntity.of(service.getById(id));
    }

    @PutMapping("{id}")
    public void putOneArticle(
            @RequestBody() ArticleDtoIn dto,
            @PathVariable Long id
            ) {
        service.updateArticle(dto, id);
    }

    @PutMapping("delete/{id}")
    public void setOneArticleNotOrderable(
            @PathVariable Long id
    ) {
        service.setArticleNotOrderable(id);
    }

    @DeleteMapping("adminDelete/{id}")
    public void deleteOneArticleAdmin(
            @PathVariable Long id
    ){
        service.deleteArticle(id);
    }
}
