package com.example.rest.controller;

import com.example.rest.dto.ArticleDtoIn;
import com.example.rest.dto.OrderCreateDtoIn;
import com.example.rest.dto.OrderDtoIn;
import com.example.rest.dto.OrderDtoOut;
import com.example.rest.entities.Order;
import com.example.rest.services.ArticleService;
import com.example.rest.services.OrderService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderService service;

    public OrderController(OrderService service) {
        this.service = service;
    }

    @PostMapping()
    public ResponseEntity<OrderDtoOut> create(
            @RequestBody() OrderCreateDtoIn dtoIn
    ) {
        try {
            service.createOrder(dtoIn);
            return new ResponseEntity<OrderDtoOut>(HttpStatusCode.valueOf(200));
        } catch (Error error) {
            return new ResponseEntity<OrderDtoOut>(HttpStatusCode.valueOf(404));
        }
    }

    @GetMapping()
    public List<OrderDtoOut> getAllOrders(

    ) {
        return service.getAll();
    }

    @GetMapping("{id}")
    public OrderDtoOut getOneOrder(
            @PathVariable Long id
    ) {
        return service.getById(id);
    }

    @PutMapping("{id}")
    public void putOneOrder(
            @RequestBody() OrderDtoIn dto,
            @PathVariable Long id
    ) {
        service.updateOrder(dto, id);
    }

    @DeleteMapping("{id}")
    public void deleteOneOrder(
        @PathVariable Long id
    ) {
        service.deleteOrder(id);
    }
}
