
# Rest Api

Here is a project to test and improve my knowledge about rest api in Java. This project is build with gradle.


## Database
Database schema for this project:

![Database schema](/ressources/Mcd.png)

## Execute the project

This project uses docker. You need to have docker installed on your computer. You can download it on this link:

https://www.docker.com

Once you installed Docker you can run the following command:

```
docker compose up -d
```

Then you need to build gradle. You can do it in your text editor.\
Finaly, you can execute the Java main.
## Api

The server is started on port 8080.\
See on:
http://localhost:8080

## Postman

To request the api, you will need Postman.\
You can install Postman on: https://www.postman.com/downloads

# Examples request and data types

## Article
Firstly, you will have to create some articles.\
To create article you will have to request as the following with Post keyword
```http request
localhost:8080/articles
```
To create an article, you will need to provide a JSON body.
Here is an example of body.
```json
{
    "name": "name of the article",
    "numberOwned": 150,
    "description": "here is a description of your product",
    "price": 5.40    
}
```
You can see the example below in Postman:

![Create article on Postman](./ressources/create-article-postman.png)

To get all the articles you can use the following request with get keyword

```http request
localhost:8080/articles
```

To get an article, you can use the following request with get keyword

```http request
localhost:8080/articles/{{id}}
```

This request will respond a list of all the articles saved in database where the status "isDeleted" is false. This means that all the product soft deleted are not showed when you've requested this.

To get also the soft deleted products, you will have to request (still get keyword) as below
```http request
localhost:8080/articles/admin
```

To update an article where {{id}} is article id with put keyword:
```http request
localhost:8080/articles/{{id}}
```

You need to provide a JSON like the following:
```json
{
  "articleId": 1,
  "name": "name",
  "numberOwned": 50,
  "description": "this is a description",
  "price": 6.30
}
```

To soft-delete an article you can run a put command as below where {{id}} is the id of your article.
```http request
localhost:8080/articles/{{id}}
```

To force delete an article run the delete request :
```http request
localhost:8080/adminDelete/{{id}}
```

## Orders

When you created a list of articles, you'll be able to create an order.
If you try to create an order without any project you will get an 404 error code.

To create orders you will have to request as the following with Post keyword
```http request
localhost:8080/orders
```

To create an order, you will need to provide a JSON body.
Here is an example of body.
```json
{
    "dateOrdered": "2023-10-05",
    "articles": [
      {
        "articleId": 1,
        "quantity": 10,
        "price": 15
      }
    ]  
}
```

To get all the orders you can use the following request with get keyword

```http request
localhost:8080/orders
```

To delete an order, you can use the following request with delete keyword:

```http request
localhost:8080/orders/{{id}}
```
